import "./index.css";
import { createInitializeMap } from "./Application/initializeMap";
import { getTile } from "./Ressources/getTile";
import { createDrawingZone } from "./UI/DrawingZone";
import { createImageFactory } from "./UI/ImageFactory";
import { getCurrentGeoLocation } from "./Ressources/getCurrentGeoLocation";
import { getTileMatrix } from "./Ressources/getTileMatrix";
import { createDrawMap } from "./Application/Commands/Functions/drawMap";
import { createMoveFollower } from "./UI/MoveFollower";
import { createSetMapLocation } from "./Application/Commands/setMapLocation";
import { createMoveMap } from "./Application/Commands/moveMap";
import { createResizeMap } from "./Application/Commands/resizeMap";
import { createZoomMap } from "./Application/Commands/zoomMap";
import { createZoomFollower } from "./UI/ZoomFollower";

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./sw.js", { scope: "." })
    .then((reg) => {
      console.log("Registration succeeded. Scope is " + reg.scope);
    })
    .catch((error) => {
      console.log("Registration failed with " + error);
    });
}

const drawingZone = createDrawingZone(
  document.getElementById("map") as HTMLCanvasElement,
  createImageFactory(),
  createMoveFollower,
  createZoomFollower,
  createImageBitmap
);
const drawMap = createDrawMap(getTile);
const initializeMap = createInitializeMap(
  getTileMatrix,
  getCurrentGeoLocation(navigator.geolocation),
  createSetMapLocation(drawMap),
  createMoveMap(drawMap),
  createResizeMap(drawMap),
  createZoomMap(drawMap, getTileMatrix),
  drawMap
);

initializeMap(drawingZone);
