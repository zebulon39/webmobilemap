import { createInitializeMap, InitializeMap } from "./initializeMap";

describe("InitializeMap", () => {
  const resizeMap = jest.fn();
  const getTileMatrix = jest.fn();
  const getCurrentGeoLocation = jest.fn();
  const setMapLocation = jest.fn();
  let initializeMap: InitializeMap;
  let drawingZone: {
    draw: jest.Mock<any, any>;
    getWidth: jest.Mock<any, any>;
    getHeight: jest.Mock<any, any>;
    addMoveListener: jest.Mock<any, any>;
    addResizeListener: jest.Mock<any, any>;
    addZoomListener: jest.Mock<any, any>;
  };
  beforeEach(() => {
    getTileMatrix.mockReturnValue({
      id: 18,
      cornerTop: 20037508,
      cornerLeft: -20037508,
      meterPerPixel: 0.597164,
      pixelByTile: 256,
    });
    drawingZone = {
      draw: jest.fn(),
      getWidth: jest.fn(),
      getHeight: jest.fn(),
      addMoveListener: jest.fn(),
      addResizeListener: jest.fn(),
      addZoomListener: jest.fn(),
    };
    initializeMap = createInitializeMap(
      getTileMatrix,
      getCurrentGeoLocation,
      setMapLocation,
      jest.fn(),
      resizeMap,
      jest.fn(),
      jest.fn()
    );
  });
  it("it resizes map when drawing zone is resized", async () => {
    await initializeMap(drawingZone);

    expect(drawingZone.addResizeListener).toHaveBeenCalled();
    const call = drawingZone.addResizeListener.mock.calls[0];
    expect(call).toBeDefined();
    expect(call[0]).toBeDefined();
    const width = 1024;
    const height = 512;
    call[0](width, height);
    expect(resizeMap).toHaveBeenCalledWith(
      expect.anything(),
      drawingZone,
      width,
      height
    );
  });
  describe("When current location is not available", () => {
    beforeEach(() => {
      getCurrentGeoLocation.mockRejectedValue("Location not available");
    });
    it("does not failed", async () => {
      await initializeMap(drawingZone);
    });
  });
});
