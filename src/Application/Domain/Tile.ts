export interface Tile {
  getBlob(): Blob;
}
export interface TileCoordinates {
  row: number;
  column: number;
}
