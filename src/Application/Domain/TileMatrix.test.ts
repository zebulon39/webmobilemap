import { createTileMatrix } from "./TileMatrix";

let getTile: jest.Mock<any, any>;

describe("#getEquivalentPoint", () => {
  it("should return the same location", () => {
    let tileMatrix = createTileMatrix({
      id: "1",
      cornerTop: 0,
      cornerLeft: 0,
      meterPerPixel: 2,
      pixelByTile: 10,
    });
    let zoomedTileMatrix = createTileMatrix({
      id: "2",
      cornerTop: 0,
      cornerLeft: 0,
      meterPerPixel: 1,
      pixelByTile: 10,
    });

    let location = zoomedTileMatrix.getEquivalentPoint(tileMatrix, {
      tileColumn: 1,
      tileRow: 1,
      xInTile: 8,
      yInTile: 8,
    });

    expect(location).toEqual({
      tileColumn: 3,
      tileRow: 3,
      xInTile: 6,
      yInTile: 6,
    });
  });
});
