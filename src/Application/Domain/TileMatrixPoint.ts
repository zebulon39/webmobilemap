export interface TileMatrixPoint {
  tileRow: number;
  tileColumn: number;
  xInTile: number;
  yInTile: number;
}
