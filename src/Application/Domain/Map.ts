import { GeoLocation } from "./GeoLocation";
import { TileMatrix } from "./TileMatrix";
import { MapTile } from "./MapTile";
import { TileMatrixPoint } from "./TileMatrixPoint";
import { calculateMapTiles } from "./Functions/calculateMapTiles";

export interface Map {
  getTileMatrixId(): number;
  getTiles(): MapTile[];
  setLocation(geoLocation: GeoLocation): void;
  move(dx: number, dy: number): void;
  resize(width: number, height: number): void;
  setTileMatrix(tileMatrix: TileMatrix): void;
}

const chamrousseCrossGeoLocation = {
  latitude: 45.125942,
  longitude: 5.902626,
};

export function createMap(
  tileMatrix: TileMatrix,
  width: number,
  height: number
): Map {
  let _tileMatrix = tileMatrix;
  let _width = width;
  let _height = height;
  let _tiles: MapTile[] = [];
  let _centerPoint: TileMatrixPoint;

  _centerPoint = _tileMatrix.getPointAt(chamrousseCrossGeoLocation);

  const _fillMap = () =>
    (_tiles = calculateMapTiles(
      _centerPoint,
      _width,
      _height,
      _tileMatrix.getPixelByTile()
    ));

  _fillMap();

  return {
    getTileMatrixId: (): number => _tileMatrix.getId(),
    getTiles: (): MapTile[] => _tiles,
    setLocation: (geoLocation: GeoLocation): void => {
      _centerPoint = _tileMatrix.getPointAt(geoLocation);
      _fillMap();
    },
    move: (dx: number, dy: number): void => {
      _centerPoint = _tileMatrix.getRelativePoint(_centerPoint, dx, dy);
      _fillMap();
    },
    resize: (width: number, height: number): void => {
      _width = width;
      _height = height;
      _fillMap();
    },
    setTileMatrix: (tileMatrix: TileMatrix) => {
      _centerPoint = tileMatrix.getEquivalentPoint(_tileMatrix, _centerPoint);
      _tileMatrix = tileMatrix;
      _fillMap();
    },
  };
}
