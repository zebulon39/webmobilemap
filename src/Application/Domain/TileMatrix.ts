import { GeoLocation } from "./GeoLocation";
import { TileMatrixPoint } from "./TileMatrixPoint";

export interface TileMatrix {
  getId(): number;
  getPixelByTile(): number;
  getMeterByPixel(): number;
  getPointAt(location: GeoLocation): TileMatrixPoint;
  getRelativePoint(
    point: TileMatrixPoint,
    dx: number,
    dy: number
  ): TileMatrixPoint;
  getEquivalentPoint(
    tileMatrix: TileMatrix,
    point: TileMatrixPoint
  ): TileMatrixPoint;
}

export function createTileMatrix(tileMatrixData: {
  id: number;
  cornerTop: number;
  cornerLeft: number;
  meterPerPixel: number;
  pixelByTile: number;
}): TileMatrix {
  var _id = tileMatrixData.id;
  var _cornerTop = tileMatrixData.cornerTop;
  var _cornerLeft = tileMatrixData.cornerLeft;
  var _meterPerPixel = tileMatrixData.meterPerPixel;
  var _pixelByTile = tileMatrixData.pixelByTile;
  return {
    getId: (): number => _id,
    getPixelByTile: () => _pixelByTile,
    getMeterByPixel: () => _meterPerPixel,
    getPointAt: (location: GeoLocation): TileMatrixPoint => {
      const { webMercatorX, webMercatorY } = convertToWebMercator(location);
      const tileMeterResolution = _pixelByTile * _meterPerPixel;
      const column = (webMercatorX - _cornerLeft) / tileMeterResolution;
      const row = (_cornerTop - webMercatorY) / tileMeterResolution;
      const xInTile = Math.round((column - Math.floor(column)) * _pixelByTile);
      const yInTile = Math.round((row - Math.floor(row)) * _pixelByTile);
      return {
        tileRow: Math.floor(row),
        tileColumn: Math.floor(column),
        xInTile,
        yInTile,
      };
    },
    getRelativePoint: (
      point: TileMatrixPoint,
      dx: number,
      dy: number
    ): TileMatrixPoint => {
      let x = point.tileColumn * _pixelByTile + point.xInTile + dx;
      let y = point.tileRow * _pixelByTile + point.yInTile + dy;
      const column = Math.floor(x / _pixelByTile);
      const row = Math.floor(y / _pixelByTile);
      return {
        tileRow: row,
        tileColumn: column,
        xInTile: x - column * _pixelByTile,
        yInTile: y - row * _pixelByTile,
      };
    },
    getEquivalentPoint: (
      tileMatrix: TileMatrix,
      point: TileMatrixPoint
    ): TileMatrixPoint => {
      let scale = tileMatrix.getMeterByPixel() / _meterPerPixel;
      let x =
        (point.tileColumn * tileMatrix.getPixelByTile() + point.xInTile) *
        scale;
      let y =
        (point.tileRow * tileMatrix.getPixelByTile() + point.yInTile) * scale;

      const column = Math.floor(x / _pixelByTile);
      const row = Math.floor(y / _pixelByTile);
      return {
        tileRow: row,
        tileColumn: column,
        xInTile: x - column * _pixelByTile,
        yInTile: y - row * _pixelByTile,
      };
    },
  };
}

function convertToWebMercator(location: GeoLocation) {
  const longitude = (location.longitude * Math.PI) / 180;
  const latitude = (location.latitude * Math.PI) / 180;
  const earthRadius = 6378137.0;

  const webMercatorX = earthRadius * longitude;
  const webMercatorY =
    earthRadius * Math.log(Math.tan(latitude / 2 + Math.PI / 4));

  return { webMercatorX, webMercatorY };
}
