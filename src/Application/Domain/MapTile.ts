export interface MapTile {
  tileRow: number;
  tileColumn: number;
  xInMap: number;
  yInMap: number;
}
