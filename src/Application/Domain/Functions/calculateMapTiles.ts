import { MapTile } from "../MapTile";
import { TileMatrixPoint } from "../TileMatrixPoint";

export function calculateMapTiles(
  centerPoint: TileMatrixPoint,
  width: number,
  height: number,
  pixelByTile: number
) {
  const _calculateTilePosition = (point: TileMatrixPoint) => ({
    x: Math.round(width / 2.0 - point.xInTile),
    y: Math.round(height / 2.0 - point.yInTile),
  });

  const _getRelativeMapTile =
    (
      tileRowColumn: { tileRow: number; tileColumn: number },
      tilePosition: { x: number; y: number }
    ) =>
    (dx: number, dy: number): MapTile => {
      const x = tilePosition.x + dx * pixelByTile;
      const y = tilePosition.y + dy * pixelByTile;

      return {
        tileRow: tileRowColumn.tileRow + dy,
        tileColumn: tileRowColumn.tileColumn + dx,
        xInMap: x,
        yInMap: y,
      };
    };

  const _getTopRow =
    (getCenterRelativeMapTile: (dx: number, dy: number) => MapTile) =>
    (distance: number) => {
      var results = [];
      const dy = -distance;
      for (let dx = -distance; dx < distance; dx++) {
        results.push(getCenterRelativeMapTile(dx, dy));
      }
      return results;
    };

  const _getRightColumn =
    (getCenterRelativeMapTile: (dx: number, dy: number) => MapTile) =>
    (distance: number) => {
      var results = [];
      const dx = distance;
      for (let dy = -distance; dy < distance; dy++) {
        results.push(getCenterRelativeMapTile(dx, dy));
      }
      return results;
    };

  const _getBottomRow =
    (getCenterRelativeMapTile: (dx: number, dy: number) => MapTile) =>
    (distance: number) => {
      var results = [];
      const dy = distance;
      for (let dx = distance; dx > -distance; dx--) {
        results.push(getCenterRelativeMapTile(dx, dy));
      }
      return results;
    };

  const _getLeftColumn =
    (getCenterRelativeMapTile: (dx: number, dy: number) => MapTile) =>
    (distance: number) => {
      var results = [];
      const dx = -distance;
      for (let dy = distance; dy > -distance; dy--) {
        results.push(getCenterRelativeMapTile(dx, dy));
      }
      return results;
    };

  const _getTileWithCenter = (
    getCenterRelativeMapTile: (dx: number, dy: number) => MapTile
  ) => getCenterRelativeMapTile(0, 0);

  const _calculateDistanceMax = (tilePosition: {
    x: number;
    y: number;
  }): number =>
    Math.max(
      Math.ceil(tilePosition.x / pixelByTile),
      Math.ceil((tilePosition.x + width) / pixelByTile),
      Math.ceil(tilePosition.y / pixelByTile),
      Math.ceil((tilePosition.y + height) / pixelByTile)
    );

  const _isTileOutsideMap = (tilePosition: { x: number; y: number }): boolean =>
    tilePosition.x + pixelByTile < 0 ||
    tilePosition.x > width ||
    tilePosition.y + pixelByTile < 0 ||
    tilePosition.y > height;

  const centerTilePosition = _calculateTilePosition(centerPoint);
  const distanceMaxInDrawingZone = _calculateDistanceMax(centerTilePosition);
  const getCenterRelativeMapTile = _getRelativeMapTile(
    { tileRow: centerPoint.tileRow, tileColumn: centerPoint.tileColumn },
    centerTilePosition
  );

  var mapTiles: MapTile[] = [];
  mapTiles.push(_getTileWithCenter(getCenterRelativeMapTile));
  for (let distance = 1; distance <= distanceMaxInDrawingZone; distance++) {
    mapTiles.push(..._getTopRow(getCenterRelativeMapTile)(distance));
    mapTiles.push(..._getRightColumn(getCenterRelativeMapTile)(distance));
    mapTiles.push(..._getBottomRow(getCenterRelativeMapTile)(distance));
    mapTiles.push(..._getLeftColumn(getCenterRelativeMapTile)(distance));
  }

  mapTiles = mapTiles.filter(
    (mapTile) =>
      !_isTileOutsideMap({
        x: mapTile.xInMap,
        y: mapTile.yInMap,
      })
  );

  return mapTiles;
}
