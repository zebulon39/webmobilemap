import { Map } from "../../Domain/Map";
import { DrawingZone } from "../../Providers/DrawingZone";
import { GetTile } from "../../Providers/GetTile";

export type DrawMap = (map: Map, drawingZone: DrawingZone) => Promise<void>;

export const createDrawMap =
  (getTile: GetTile): DrawMap =>
  async (map: Map, drawingZone: DrawingZone) => {
    map.getTiles().forEach(async (mapTile) => {
      const tile = await getTile(map.getTileMatrixId(), {
        row: mapTile.tileRow,
        column: mapTile.tileColumn,
      });
      await drawingZone.draw(tile.getBlob(), mapTile.xInMap, mapTile.yInMap);
    });
  };
