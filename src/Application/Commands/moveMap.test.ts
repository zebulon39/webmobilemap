import { MoveMap, createMoveMap } from "./moveMap";
import { Map } from "../Domain/Map";
import { DrawingZone } from "../Providers/DrawingZone";

describe("MoveMap", () => {
  let mockDrawMap: jest.Mock<Promise<void>, [Map, DrawingZone]>;
  let moveMap: MoveMap;
  let mockMap: Map;
  let mockDrawingZone: DrawingZone;

  beforeEach(() => {
    mockDrawMap = jest.fn().mockResolvedValue(undefined);
    moveMap = createMoveMap(mockDrawMap);

    mockMap = jest.genMockFromModule("../Domain/Map");
    mockMap.move = jest.fn();

    mockDrawingZone = jest.genMockFromModule("../Providers/DrawingZone");
  });

  it("should call map.move with the provided dx and dy", async () => {
    await moveMap(mockMap, mockDrawingZone, 1, -1);

    expect(mockMap.move).toHaveBeenCalledWith(1, -1);
  });

  it("should call drawMap with the updated map and drawing zone", async () => {
    await moveMap(mockMap, mockDrawingZone, 1, -1);

    expect(mockDrawMap).toHaveBeenCalledWith(mockMap, mockDrawingZone);
  });

  it("should complete without error", async () => {
    const result = moveMap(mockMap, mockDrawingZone, 1, -1);

    await expect(result).resolves.toBeUndefined();
  });
});
