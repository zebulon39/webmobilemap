import { GeoLocation } from "../Domain/GeoLocation";
import { Map } from "../Domain/Map";
import { DrawMap } from "./Functions/drawMap";
import { DrawingZone } from "../Providers/DrawingZone";

export type SetMapLocation = (
  map: Map,
  drawingZone: DrawingZone,
  geoLocation: GeoLocation
) => Promise<void>;

export const createSetMapLocation =
  (drawMap: DrawMap): SetMapLocation =>
  async (map: Map, drawingZone: DrawingZone, geoLocation: GeoLocation) => {
    map.setLocation(geoLocation);
    await drawMap(map, drawingZone);
  };
