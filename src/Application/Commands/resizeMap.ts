import { Map } from "../Domain/Map";
import { DrawMap } from "./Functions/drawMap";
import { DrawingZone } from "../Providers/DrawingZone";

export type ResizeMap = (
  map: Map,
  drawingZone: DrawingZone,
  width: number,
  height: number
) => Promise<void>;

export const createResizeMap =
  (drawMap: DrawMap): ResizeMap =>
  async (map: Map, drawingZone: DrawingZone, width: number, height: number) => {
    map.resize(width, height);
    await drawMap(map, drawingZone);
  };
