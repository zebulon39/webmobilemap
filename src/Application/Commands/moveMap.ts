import { Map } from "../Domain/Map";
import { DrawMap } from "./Functions/drawMap";
import { DrawingZone } from "../Providers/DrawingZone";

export type MoveMap = (
  map: Map,
  drawingZone: DrawingZone,
  dx: number,
  dy: number
) => Promise<void>;

export const createMoveMap =
  (drawMap: DrawMap): MoveMap =>
  async (map: Map, drawingZone: DrawingZone, dx: number, dy: number) => {
    map.move(dx, dy);
    await drawMap(map, drawingZone);
  };
