import { Map } from "../Domain/Map";
import { createTileMatrix } from "../Domain/TileMatrix";
import { DrawingZone } from "../Providers/DrawingZone";
import { GetTileMatrix } from "../Providers/GetTileMatrix";
import { DrawMap } from "./Functions/drawMap";

export type ZoomMap = (
  map: Map,
  drawingZone: DrawingZone,
  direction: "in" | "out"
) => Promise<void>;

export const createZoomMap =
  (drawMap: DrawMap, getTileMatrix: GetTileMatrix): ZoomMap =>
  async (map: Map, drawingZone: DrawingZone, direction: "in" | "out") => {
    let tileMatrixId = map.getTileMatrixId() + (direction === "in" ? 1 : -1);
    map.setTileMatrix(createTileMatrix(await getTileMatrix(tileMatrixId)));
    await drawMap(map, drawingZone);
  };
