import { createDrawMap } from "./Functions/drawMap";
import { GeoLocation } from "../Domain/GeoLocation";
import { createSetMapLocation, SetMapLocation } from "./setMapLocation";
import { createMap } from "../Domain/Map";
import { createTileMatrix } from "../Domain/TileMatrix";

let drawingZone: {
  draw: jest.Mock<any, any>;
  getWidth: jest.Mock<any, any>;
  getHeight: jest.Mock<any, any>;
  addMoveListener: jest.Mock<any, any>;
  addResizeListener: jest.Mock<any, any>;
  addZoomListener: jest.Mock<any, any>;
};

let getTile: jest.Mock<any, any>;
let setMapLocation: SetMapLocation;
beforeEach(async () => {
  drawingZone = {
    draw: jest.fn(),
    getWidth: jest.fn(),
    getHeight: jest.fn(),
    addMoveListener: jest.fn(),
    addResizeListener: jest.fn(),
    addZoomListener: jest.fn(),
  };
  getTile = jest.fn();
  setMapLocation = createSetMapLocation(createDrawMap(getTile));
});

describe("setMapLocation", () => {
  beforeEach(async () => {});

  it("it center map at given geo location", async () => {
    //Given
    const tileMatrixId = 18;
    const currentLocation: GeoLocation = {
      latitude: 48.805639,
      longitude: 2.478917,
    };
    const drawingZoneSize = {
      width: 1024,
      height: 512,
    };
    const blob = {} as Blob;

    const expectGetTileAndDraw = (
      row: number,
      column: number,
      x: number,
      y: number
    ) => {
      expect(getTile).toHaveBeenCalledWith(tileMatrixId, {
        row,
        column,
      });
      expect(drawingZone.draw).toHaveBeenCalledWith(blob, x, y);
    };

    drawingZone.getWidth.mockReturnValue(drawingZoneSize.width);
    drawingZone.getHeight.mockReturnValue(drawingZoneSize.height);
    getTile.mockReturnValue(
      Promise.resolve({
        getBlob: () => {
          return blob;
        },
      })
    );

    const map = createMap(
      createTileMatrix({
        id: tileMatrixId,
        cornerTop: 20037508,
        cornerLeft: -20037508,
        meterPerPixel: 0.597164,
        pixelByTile: 256,
      }),
      drawingZoneSize.width,
      drawingZoneSize.height
    );

    //When
    await setMapLocation(map, drawingZone, currentLocation);

    //Then
    // Centered tile
    expectGetTileAndDraw(90241, 132877, 473, 155);
    // Up row
    expectGetTileAndDraw(90240, 132876, 217, -101);
    expectGetTileAndDraw(90240, 132877, 473, -101);
    // Right column
    expectGetTileAndDraw(90240, 132878, 729, -101);
    expectGetTileAndDraw(90241, 132878, 729, 155);
    // Bottomn row
    expectGetTileAndDraw(90242, 132878, 729, 411);
    expectGetTileAndDraw(90242, 132877, 473, 411);
    // Left column
    expectGetTileAndDraw(90242, 132876, 217, 411);
    expectGetTileAndDraw(90241, 132876, 217, 155);
    // Left column aloof  2 tiles
    expectGetTileAndDraw(90240, 132875, -39, -101);
    expectGetTileAndDraw(90241, 132875, -39, 155);
    expectGetTileAndDraw(90242, 132875, -39, 411);
    // Right column aloof 2 tiles
    expectGetTileAndDraw(90240, 132879, 985, -101);
    expectGetTileAndDraw(90241, 132879, 985, 155);
    expectGetTileAndDraw(90242, 132879, 985, 411);
    // No other tiles (outside the map) should be drawn
    expect(drawingZone.draw).toBeCalledTimes(15);
  });
});
