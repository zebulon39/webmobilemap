export interface TileMatrixData {
  id: number;
  cornerTop: number;
  cornerLeft: number;
  meterPerPixel: number;
  pixelByTile: number;
}

export type GetTileMatrix = (id: number) => Promise<TileMatrixData>;
