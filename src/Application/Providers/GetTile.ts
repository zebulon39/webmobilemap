import { Tile, TileCoordinates } from "../Domain/Tile";

export type GetTile = (
  tileMatrixId: string,
  coordinates: TileCoordinates
) => Promise<Tile>;
