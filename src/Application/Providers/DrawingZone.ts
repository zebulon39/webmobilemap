export type MoveListener = (dx: number, dy: number) => Promise<void>;
export type ResizeListener = (width: number, height: number) => Promise<void>;
export type ZoomListener = (direction: "in" | "out") => Promise<void>;

export interface DrawingZone {
  draw(imageBlob: Blob, x: number, y: number): Promise<void>;
  getWidth(): number;
  getHeight(): number;
  addMoveListener(listener: MoveListener): void;
  addResizeListener(resizeListener: ResizeListener): void;
  addZoomListener(zoomListener: ZoomListener): void;
}
