import { GeoLocation } from "../Domain/GeoLocation";

export type GetGeoLocation = () => Promise<GeoLocation>;
