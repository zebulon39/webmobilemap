import { createMap } from "./Domain/Map";
import { createTileMatrix } from "./Domain/TileMatrix";
import { DrawMap } from "./Commands/Functions/drawMap";
import { MoveMap } from "./Commands/moveMap";
import { DrawingZone } from "./Providers/DrawingZone";
import { GetGeoLocation } from "./Providers/GetGeoLocation";
import { GetTileMatrix } from "./Providers/GetTileMatrix";
import { ResizeMap } from "./Commands/resizeMap";
import { SetMapLocation } from "./Commands/setMapLocation";
import { ZoomMap } from "./Commands/zoomMap";

export { createInitializeMap };
export type InitializeMap = (drawingZone: DrawingZone) => Promise<void>;

const defaultTileMatrixId = 15;

const createInitializeMap =
  (
    getTileMatrix: GetTileMatrix,
    getCurrentGeoLocation: GetGeoLocation,
    setMapLocation: SetMapLocation,
    moveMap: MoveMap,
    resizeMap: ResizeMap,
    zoomMap: ZoomMap,
    drawMap: DrawMap
  ): InitializeMap =>
  async (drawingZone: DrawingZone): Promise<void> => {
    const map = createMap(
      createTileMatrix(await getTileMatrix(defaultTileMatrixId)),
      drawingZone.getWidth(),
      drawingZone.getHeight()
    );

    try {
      setMapLocation(map, drawingZone, await getCurrentGeoLocation());
    } catch (error) {
      drawMap(map, drawingZone);
    }

    drawingZone.addMoveListener(async (dx, dy) => {
      await moveMap(map, drawingZone, dx, dy);
    });
    drawingZone.addResizeListener(async (width, height) => {
      await resizeMap(map, drawingZone, width, height);
    });
    drawingZone.addZoomListener(async (direction) => {
      await zoomMap(map, drawingZone, direction);
    });
  };
