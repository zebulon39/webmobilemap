import { StartListener } from "./MoveFollower";

export type ZoomListener = (zoom: number) => Promise<void>;
export type ZoomingListener = (scale: number) => Promise<void>;

export type CreateZoomFollower = (
  element: HTMLElement,
  startListener: StartListener,
  zoomingListener: ZoomingListener,
  zoomListener: ZoomListener
) => void;

export function createZoomFollower(
  element: HTMLElement,
  startListener: StartListener,
  zoomingListener: ZoomingListener,
  zoomListener: ZoomListener
) {
  createTouchZoomFollower(
    element,
    startListener,
    zoomingListener,
    zoomListener
  );
  createWheelZoomFollower(element, zoomListener);
}

function createTouchZoomFollower(
  element: HTMLElement,
  startListener: StartListener,
  zoomingListener: ZoomingListener,
  zoomListener: ZoomListener
) {
  let lastTouchDistance = 0;
  let touchDistance = 0;

  element.addEventListener("touchstart", async (event) => {
    if (event.touches.length === 2) {
      event.preventDefault();
      lastTouchDistance = getTouchDistance(event.touches);
      touchDistance = lastTouchDistance;
      await startListener();
    }
  });

  element.addEventListener("touchmove", async (event) => {
    if (event.touches.length === 2) {
      event.preventDefault();
      lastTouchDistance = getTouchDistance(event.touches);
      await zoomingListener(lastTouchDistance / touchDistance);
    }
  });

  element.addEventListener("touchend", async (event) => {
    if (event.touches.length === 1) {
      event.preventDefault();
      if (touchDistance > lastTouchDistance) {
        await zoomListener(1);
      } else if (touchDistance < lastTouchDistance) {
        await zoomListener(-1);
      }
    }
  });
}

function createWheelZoomFollower(
  element: HTMLElement,
  zoomListener: ZoomListener
) {
  element.addEventListener("wheel", async (event) => {
    event.preventDefault();
    await zoomListener(event.deltaY > 0 ? 1 : -1);
  });
}

function getTouchDistance(touches: TouchList) {
  const x1 = touches[0].clientX;
  const y1 = touches[0].clientY;
  const x2 = touches[1].clientX;
  const y2 = touches[1].clientY;

  return Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
}
