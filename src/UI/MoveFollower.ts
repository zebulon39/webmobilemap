export type MouseListener = (dx: number, dy: number) => Promise<void>;
export type StartListener = () => Promise<void>;

export type CreateMoveFollower = (
  element: HTMLElement,
  startListener: StartListener,
  movingListener: MouseListener,
  moveListener: MouseListener
) => void;

export function createMoveFollower(
  element: HTMLElement,
  startListener: StartListener,
  movingListener: MouseListener,
  moveListener: MouseListener
) {
  createMouseMoveFollower(element, startListener, movingListener, moveListener);
  createTouchMoveFollower(element, startListener, movingListener, moveListener);
}

function createMouseMoveFollower(
  element: HTMLElement,
  startListener: StartListener,
  movingListener: MouseListener,
  moveListener: MouseListener
) {
  let mouving: boolean = false;
  let start: { clientX: number; clientY: number };

  element.addEventListener("mousedown", async (mouseDown: MouseEvent) => {
    mouving = true;
    start = { clientX: mouseDown.clientX, clientY: mouseDown.clientY };
    await startListener();
  });

  element.addEventListener("mousemove", async (mouseMove: MouseEvent) => {
    if (mouving) {
      await movingListener(
        mouseMove.clientX - start.clientX,
        mouseMove.clientY - start.clientY
      );
    }
  });

  element.addEventListener("mouseup", async (mouseUp: MouseEvent) => {
    mouving = false;
    await moveListener(
      start.clientX - mouseUp.clientX,
      start.clientY - mouseUp.clientY
    );
  });
}

function createTouchMoveFollower(
  element: HTMLElement,
  startListener: StartListener,
  movingListener: MouseListener,
  moveListener: MouseListener
) {
  let mouving: boolean = false;
  let start: { clientX: number; clientY: number };

  element.addEventListener("touchstart", async (touchEvent: TouchEvent) => {
    if (touchEvent.touches.length === 1) {
      touchEvent.preventDefault();
      mouving = true;
      let touch = touchEvent.changedTouches[0];
      start = { clientX: touch.clientX, clientY: touch.clientY };
      await startListener();
    } else {
      mouving = false;
    }
  });

  element.addEventListener("touchmove", async (touchEvent: TouchEvent) => {
    if (touchEvent.touches.length === 1) {
      if (mouving) {
        touchEvent.preventDefault();
        let touch = touchEvent.changedTouches[0];
        await movingListener(
          touch.clientX - start.clientX,
          touch.clientY - start.clientY
        );
      }
    }
  });

  element.addEventListener("touchend", async (touchEvent: TouchEvent) => {
    if (mouving && touchEvent.touches.length === 0) {
      touchEvent.preventDefault();
      mouving = false;
      let touch = touchEvent.changedTouches[0];
      await moveListener(
        start.clientX - touch.clientX,
        start.clientY - touch.clientY
      );
    }
  });
}
