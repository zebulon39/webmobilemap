export { ImageFactory, createImageFactory };

// no tests made as ImageFactory use image.onload
// tests would require node-canvas and jtest setting "testEnvironmentOptions": { "resources": "usable" }

interface ImageFactory {
  create: (blob: Blob) => Promise<HTMLImageElement>;
}

const createImageFactory = (): ImageFactory => ({
  create: async (blob: Blob) =>
    new Promise((resolve) => {
      const image = new Image();
      image.onload = () => {
        URL.revokeObjectURL(image.src);
        resolve(image);
      };
      image.src = URL.createObjectURL(blob);
    }),
});
