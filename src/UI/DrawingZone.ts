import {
  DrawingZone,
  MoveListener,
  ResizeListener,
  ZoomListener,
} from "../Application/Providers/DrawingZone";
import { ImageFactory } from "./ImageFactory";
import { CreateMoveFollower } from "./MoveFollower";
import { CreateZoomFollower } from "./ZoomFollower";

export function createDrawingZone(
  canvas: HTMLCanvasElement,
  imageFactory: ImageFactory,
  createMoveFollower: CreateMoveFollower,
  createZoomFollower: CreateZoomFollower,
  createImageBitmap: (canvas: HTMLCanvasElement) => Promise<ImageBitmap>
): DrawingZone {
  const _moveListeners: MoveListener[] = [];
  const _resizeListeners: ResizeListener[] = [];
  const _zoomListeners: ZoomListener[] = [];

  const _context = canvas?.getContext("2d");
  if (!_context) {
    throw new Error("no canvas context to create drawing zone");
  }

  {
    let _imageData: ImageData;
    createMoveFollower(
      canvas,
      async () => {
        //TODO https://html.spec.whatwg.org/multipage/canvas.html#concept-canvas-will-read-frequently
        _imageData = _context.getImageData(0, 0, canvas.width, canvas.height);
      },
      async (dx, dy) => {
        _context.clearRect(0, 0, canvas.width, canvas.height);
        _context.putImageData(_imageData, dx, dy);
      },
      async (dx, dy) => {
        _moveListeners.forEach(async (listener) => await listener(dx, dy));
      }
    );
  }

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  window.addEventListener("resize", async () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    _resizeListeners.forEach(async (listener) => {
      await listener(canvas.width, canvas.height);
    });
  });

  {
    let _imageBitmap: ImageBitmap;
    createZoomFollower(
      canvas,
      async () => {
        _imageBitmap = await createImageBitmap(canvas);
      },
      async (scale: number) => {
        const newWidth = _imageBitmap.width * scale;
        const newHeight = _imageBitmap.height * scale;
        const x = (canvas.width - newWidth) / 2;
        const y = (canvas.height - newHeight) / 2;

        _context.clearRect(0, 0, canvas.width, canvas.height);
        _context.drawImage(_imageBitmap, x, y, newWidth, newHeight);
      },
      async (zoom: number) => {
        _zoomListeners.forEach(async (listener) => {
          await listener(zoom > 0 ? "out" : "in");
        });
      }
    );
  }

  return {
    draw: async function (blob: Blob, x: number, y: number): Promise<void> {
      const image = await imageFactory.create(blob);
      _context.drawImage(image, x, y);
    },
    getWidth: () => canvas.width,
    getHeight: () => canvas.height,
    addMoveListener: (listener: MoveListener) => _moveListeners.push(listener),
    addResizeListener: (listener: ResizeListener) =>
      _resizeListeners.push(listener),
    addZoomListener: (listener: ZoomListener) => _zoomListeners.push(listener),
  };
}
