import { when } from "jest-when";
import { createDrawingZone } from "../DrawingZone";
import {
  canvas,
  context,
  drawingZone,
  initTestDrawingZone,
  imageFactory,
  resizeListener,
  windowAddEventListener,
  getListeners,
  callListeners,
  callResizeListeners,
} from "./DrawingZone.testhelper";
import { createMoveFollower } from "../MoveFollower";
import { createZoomFollower } from "../ZoomFollower";

beforeEach(() => {
  initTestDrawingZone();
});

describe("createDrawingZone", () => {
  it("it fails if given canvas is invalid", () => {
    expect(() =>
      createDrawingZone(
        undefined as unknown as HTMLCanvasElement,
        imageFactory,
        createMoveFollower,
        createZoomFollower,
        async (_) => ({} as ImageBitmap)
      )
    ).toThrowError();
  });
  it("Resize canvas to window inner dimension", () => {
    expect(drawingZone.getWidth()).toBe(window.innerWidth);
    expect(drawingZone.getHeight()).toBe(window.innerHeight);
  });
});

describe("drawImage", () => {
  it("it creates image from blob and draws it on canvas at given position", async () => {
    const blob = { blob: "blob" } as unknown as Blob;
    const image = { image: "image" } as unknown as HTMLImageElement;
    const dx = 1;
    const dy = 2;
    when(imageFactory.create).calledWith(blob).mockReturnValue(image);

    await drawingZone.draw(blob, dx, dy);

    expect(imageFactory.create).toHaveBeenCalled();
    expect(context.drawImage).toHaveBeenCalledWith(image, dx, dy);
  });
});

describe("getWidth", () => {
  it("it returns canvas width", () => {
    canvas.width = 512;
    const width = drawingZone.getWidth();
    expect(width).toEqual(canvas.width);
  });
});

describe("getHeight", () => {
  it("it returns canvas height", () => {
    canvas.height = 2024;
    const height = drawingZone.getHeight();
    expect(height).toEqual(canvas.height);
  });
});

describe("When canvas is resized because containing window is resized", () => {
  beforeEach(() => {
    callResizeListeners(windowAddEventListener);
  });
  it("it resizes and warns the rest of the world", () => {
    expect(drawingZone.getWidth()).toBe(window.innerWidth);
    expect(drawingZone.getHeight()).toBe(window.innerHeight);
    expect(resizeListener).toHaveBeenCalled();
  });
});
