import {
  canvas,
  context,
  imageData,
  initTestDrawingZone,
  moveListener,
  addEventListener,
  callTouchListeners,
  callMouseListeners,
} from "./DrawingZone.testhelper";

beforeEach(async () => {
  initTestDrawingZone();
});

describe("when mouse down on canvas", () => {
  beforeEach(async () => {
    callMouseListeners("mousedown", { clientX: 10, clientY: 20 });
  });

  describe("when mouse move", () => {
    beforeEach(async () => {
      callMouseListeners("mousemove", { clientX: 15, clientY: 30 });
    });

    it("it draws content image moved", () => {
      expect(context.clearRect).toBeCalledWith(
        0,
        0,
        canvas.width,
        canvas.height
      );
      expect(context.putImageData).toBeCalledWith(imageData, 5, 10);
    });
  });

  describe("when mouse up", () => {
    beforeEach(async () => {
      callMouseListeners("mouseup", { clientX: 25, clientY: 40 });
    });
    it("it calls move listener", () => {
      expect(moveListener).toHaveBeenCalledWith(-15, -20);
    });
  });
});

describe("when touch start on canvas", () => {
  beforeEach(async () => {
    callTouchListeners(addEventListener, "touchstart", [
      { clientX: 10, clientY: 20 } as Touch,
    ]);
  });

  describe("when touch move", () => {
    beforeEach(async () => {
      callTouchListeners(addEventListener, "touchmove", [
        { clientX: 15, clientY: 30 } as Touch,
      ]);
    });

    it("it draws content image moved", () => {
      expect(context.clearRect).toBeCalledWith(
        0,
        0,
        canvas.width,
        canvas.height
      );
      expect(context.putImageData).toBeCalledWith(imageData, 5, 10);
    });

    describe("when touch ends", () => {
      beforeEach(async () => {
        callTouchListeners(
          addEventListener,
          "touchend",
          [],
          [{ clientX: 25, clientY: 40 } as Touch]
        );
      });
      it("it calls move listener", () => {
        expect(moveListener).toHaveBeenCalledWith(-15, -20);
      });
    });
  });
});
