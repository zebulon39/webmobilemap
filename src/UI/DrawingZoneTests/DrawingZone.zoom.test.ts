import {
  addEventListener,
  callTouchListeners,
  callWheelListeners,
  canvas,
  context,
  imageBitmap,
  initTestDrawingZone,
  zoomListener,
} from "./DrawingZone.testhelper";

beforeEach(() => {
  initTestDrawingZone();
});

describe("when 2 touches start on canvas", () => {
  beforeEach(async () => {
    callTouchListeners(addEventListener, "touchstart", [
      { clientX: 10, clientY: 20 },
      { clientX: 20, clientY: 20 },
    ] as Touch[]);
  });

  describe("when touches move", () => {
    beforeEach(async () => {
      callTouchListeners(addEventListener, "touchmove", [
        { clientX: 5, clientY: 20 },
        { clientX: 25, clientY: 20 },
      ] as Touch[]);
    });

    it("should draw content image, centered and scaled proportionally  with touches's distance gap", () => {
      expect(context.clearRect).toBeCalledWith(
        0,
        0,
        canvas.width,
        canvas.height
      );
      expect(context.drawImage).toBeCalledWith(
        imageBitmap,
        -canvas.width / 2,
        -canvas.height / 2,
        canvas.width * 2,
        canvas.height * 2
      );
    });

    describe("when at least one of the 2 touches ends", () => {
      beforeEach(async () => {
        callTouchListeners(addEventListener, "touchend", [
          { clientX: 5, clientY: 20 },
        ] as Touch[]);
      });

      it("should call zoom listener", () => {
        expect(zoomListener).toHaveBeenCalledWith("in");
      });
    });
  });
});

describe("when mouse wheel is turn up", () => {
  beforeEach(async () => {
    callWheelListeners(addEventListener, { deltaY: 1 });
  });

  it("should call zoom listener with 'out'", () => {
    expect(zoomListener).toHaveBeenCalledWith("out");
  });
});

describe("when mouse wheel is turn down", () => {
  beforeEach(async () => {
    callWheelListeners(addEventListener, { deltaY: -1 });
  });

  it("should call zoom listener with 'in'", () => {
    expect(zoomListener).toHaveBeenCalledWith("in");
  });
});
