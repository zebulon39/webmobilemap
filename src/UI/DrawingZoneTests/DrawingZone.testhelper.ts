import { DrawingZone } from "../../Application/Providers/DrawingZone";
import { createDrawingZone } from "../DrawingZone";
import { createMoveFollower } from "../MoveFollower";
import { createZoomFollower } from "../ZoomFollower";

export let imageFactory: { create: jest.Mock<any, any> };
export let addEventListener: jest.Mock<any, any>;
export let windowAddEventListener: jest.Mock<any, any>;
export let canvas: HTMLCanvasElement;
export let context: {
  drawImage: jest.Mock<any, any>;
  getImageData: jest.Mock<any, any>;
  putImageData: jest.Mock<any, any>;
  clearRect: jest.Mock<any, any>;
};
export let drawingZone: DrawingZone;
export let moveListener: jest.Mock<any, any>;
export let resizeListener: jest.Mock<any, any>;
export let zoomListener: jest.Mock<any, any>;
export let imageData: ImageData;
export let imageBitmap = {
  width: window.innerWidth,
  height: window.innerHeight,
} as ImageBitmap;
const imageBitmapPromise = Promise.resolve(imageBitmap);

export function initTestDrawingZone() {
  imageFactory = {
    create: jest.fn(),
  };
  context = {
    drawImage: jest.fn(),
    getImageData: jest.fn(() => imageData),
    putImageData: jest.fn(),
    clearRect: jest.fn(),
  };
  windowAddEventListener = jest.fn();
  window.addEventListener = windowAddEventListener;
  addEventListener = jest.fn();
  canvas = {
    getContext: () => context,
    addEventListener: addEventListener,
  } as unknown as HTMLCanvasElement;
  drawingZone = createDrawingZone(
    canvas,
    imageFactory,
    createMoveFollower,
    createZoomFollower,
    (_) => imageBitmapPromise
  );

  moveListener = jest.fn();
  resizeListener = jest.fn();
  zoomListener = jest.fn();
  drawingZone.addMoveListener(moveListener);
  drawingZone.addResizeListener(resizeListener);
  drawingZone.addZoomListener(zoomListener);

  imageData = {} as ImageData;
  imageBitmap = {
    width: window.innerWidth,
    height: window.innerHeight,
  } as ImageBitmap;
}

export function callResizeListeners(addEventMock: jest.Mock<any, any>) {
  callListeners(getListeners(addEventMock, "resize"), {});
}

export function callMouseListeners(type: string, event: Partial<WheelEvent>) {
  callListeners(
    getListeners(addEventListener, type),
    new MouseEvent(type, event)
  );
}

export function callTouchListeners(
  addEventMock: jest.Mock<any, any>,
  type: string,
  touches: Touch[],
  changedTouches?: Touch[]
) {
  callListeners(
    getListeners(addEventMock, type),
    new TouchEvent(type, {
      touches,
      changedTouches: changedTouches ?? touches,
    })
  );
}

export function callWheelListeners(
  addEventMock: jest.Mock<any, any>,
  event: Partial<WheelEvent>
) {
  const eventType = "wheel";
  callListeners(
    getListeners(addEventMock, eventType),
    new WheelEvent(eventType, event)
  );
}

export function callListeners(listeners: any[], event: any) {
  listeners.forEach(async (listener) => await listener(event));
}

export function getListeners(addEventMock: jest.Mock<any, any>, type: string) {
  let calls = addEventMock.mock.calls.filter((c) => c[0] === type);
  expect(calls).toBeDefined();
  return calls.map((call) => {
    expect(call[1]).toBeDefined();
    return call[1];
  });
}
