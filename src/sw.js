self.addEventListener("install", (event) => {
  event.waitUntil(
    caches.open("v1").then((cache) => {
      return cache.addAll([
        "./",
        "./index.html",
        "./bundle.js",
        "./icon.png",
        "./no-wifi.png",
        "./manifest.json",
      ]);
    })
  );
});

const putInCache = async (request, response) => {
  const cache = await caches.open("v1");
  await cache.put(request, response);
};

const fallBack = async (fallbackUrl) => {
  const fallbackResponse = await caches.match(fallbackUrl);
  if (fallbackResponse) {
    return fallbackResponse;
  }
  return new Response("Network error happened", {
    status: 408,
    headers: { "Content-Type": "text/plain" },
  });
};

const cacheFirst = async ({ request, fallbackUrl }) => {
  const responseFromCache = await caches.match(request);
  if (responseFromCache) {
    return responseFromCache;
  }

  try {
    const responseFromNetwork = await fetch(request);
    putInCache(request, responseFromNetwork.clone());
    return responseFromNetwork;
  } catch (error) {
    return fallBack(fallbackUrl);
  }
};

const networkFirst = async ({ request, fallbackUrl }) => {
  try {
    const response = await fetch(request);
    const cache = await caches.open("v1");
    cache.put(request, response.clone());
    return response;
  } catch (error) {
    const cached = await caches.match(request);
    return cached || caches.match(fallbackUrl);
  }
};

self.addEventListener("fetch", (event) => {
  event.respondWith(
    event.request.url.includes("wxs.ign.fr")
      ? cacheFirst({
          request: event.request,
          fallbackUrl: "./no-wifi.png",
        })
      : networkFirst({ request: event.request, fallbackUrl: "./no-wifi.png" })
  );
});
