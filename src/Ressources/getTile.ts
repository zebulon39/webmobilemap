export { getTile };
import Axios from "axios";
import { GetTile } from "../Application/Providers/GetTile";
import { Tile, TileCoordinates } from "../Application/Domain/Tile";

const getTile: GetTile = async (
  tileMatrixId: string,
  coordinates: TileCoordinates
): Promise<Tile> => {
  const response = await Axios.get<Blob>(
    "https://wxs.ign.fr/io21v55kj0lbj44b9bvq9xtg/wmts",
    {
      params: {
        VERSION: "1.0.0",
        LAYER: "GEOGRAPHICALGRIDSYSTEMS.MAPS",
        EXCEPTIONS: "text/xml",
        FORMAT: "image/jpeg",
        SERVICE: "WMTS",
        REQUEST: "GetTile",
        STYLE: "normal",
        TILEMATRIXSET: "PM",
        TILEMATRIX: tileMatrixId,
        TILEROW: coordinates.row,
        TILECOL: coordinates.column,
      },
      responseType: "blob",
      auth: {
        username: "bruno.baudru",
        password: "NxuhOrXdXJoU43hmyWPi",
      },
    }
  );
  return {
    getBlob: () => response.data,
  };
};
