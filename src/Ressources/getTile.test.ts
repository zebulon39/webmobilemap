import { getTile } from "./getTile";
import Axios from "axios";

jest.mock("axios");

describe("getTile", () => {
  it("calls ign wmts GetTile service and returns tile's data", async () => {
    const blob = {};
    const tileMatrixId = "18";
    const tileCoordinates = { row: 1, column: 2 };
    (<jest.Mock<any, any>>Axios.get).mockReturnValueOnce({ data: blob });
    const tile = await getTile(tileMatrixId, tileCoordinates);
    expect(Axios.get).toHaveBeenCalledWith(
      "https://wxs.ign.fr/io21v55kj0lbj44b9bvq9xtg/wmts",
      {
        auth: { password: "NxuhOrXdXJoU43hmyWPi", username: "bruno.baudru" },
        params: {
          EXCEPTIONS: "text/xml",
          FORMAT: "image/jpeg",
          LAYER: "GEOGRAPHICALGRIDSYSTEMS.MAPS",
          REQUEST: "GetTile",
          SERVICE: "WMTS",
          STYLE: "normal",
          TILECOL: tileCoordinates.column,
          TILEMATRIX: tileMatrixId,
          TILEMATRIXSET: "PM",
          TILEROW: tileCoordinates.row,
          VERSION: "1.0.0",
        },
        responseType: "blob",
      }
    );
    expect(tile.getBlob()).toBe(blob);
  });
});
