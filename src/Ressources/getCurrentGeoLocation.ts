export { getCurrentGeoLocation };

const getCurrentGeoLocation = (provider: Geolocation) => {
  return () =>
    new Promise<{ latitude: number; longitude: number }>((resolve, reject) => {
      provider.getCurrentPosition(
        (position) =>
          resolve({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          }),
        (error) => reject(error),
        { enableHighAccuracy: true }
      );
    });
};
