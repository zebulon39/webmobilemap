import { getCurrentGeoLocation } from "./getCurrentGeoLocation";

describe("getGeoLocation", () => {
  let provider: Geolocation;
  beforeEach(() => {
    provider = {
      getCurrentPosition: (
        successCallback: PositionCallback,
        errorCallback?: PositionErrorCallback,
        options?: PositionOptions
      ): void => {
        if (options?.enableHighAccuracy) {
          successCallback({
            coords: {
              latitude: 2.54,
              longitude: 45.1,
            },
            timestamp: 1,
          } as GeolocationPosition);
        }
      },
    } as Geolocation;
  });

  it("it returns current high accurracy position as a Promise", async () => {
    const geoLocation = await getCurrentGeoLocation(provider)();
    expect(geoLocation).toEqual({ latitude: 2.54, longitude: 45.1 });
  });
});
