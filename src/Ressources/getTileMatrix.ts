import { GetTileMatrix } from "../Application/Providers/GetTileMatrix";
import { getCapabilities } from "./getCapabilities";

export { getTileMatrix };

const SUPPOSED_PIXEL_BY_TILE = 256;
const WMTS_ARBITRARY_PIXEL_SIZE_IN_MM = 0.28;
const MIN_ID = 3;
const MAX_ID = 18;

const getTileMatrix: GetTileMatrix = async (id: number) => {
  const capabilities = await getCapabilities();

  const tileMatrixSet = capabilities.Contents.TileMatrixSet.find(
    (s) => s["ows:Identifier"] === "PM"
  );
  if (!tileMatrixSet) {
    throw new Error('no "PM" tilematrix set in Capabilities');
  }

  if (MIN_ID > id || id > MAX_ID) {
    throw new Error(`tilematrix id ${id} is out of range for map display`);
  }

  const tileMatrix = tileMatrixSet.TileMatrix.find(
    (m) => m["ows:Identifier"] === id
  );

  if (!tileMatrix) {
    throw new Error(`no tilematrix with id ${id} in Capabilities`);
  }

  if (
    tileMatrix.TileHeight !== tileMatrix.TileWidth ||
    tileMatrix.TileHeight !== SUPPOSED_PIXEL_BY_TILE
  ) {
    throw new Error("TileMatrix width or height not equals both to 256");
  }

  const topLeftStrings = tileMatrix.TopLeftCorner.split(" ");

  return {
    id: id,
    cornerTop: parseFloat(topLeftStrings[1]),
    cornerLeft: parseFloat(topLeftStrings[0]),
    meterPerPixel:
      (parseFloat(tileMatrix.ScaleDenominator) *
        WMTS_ARBITRARY_PIXEL_SIZE_IN_MM) /
      1000.0,
    pixelByTile: SUPPOSED_PIXEL_BY_TILE,
  };
};
