import Axios from "axios";
import { XMLParser } from "fast-xml-parser";

export const getCapabilities = async (): Promise<CapabilitiesDto> => {
  const response = await Axios.get<string>(
    "https://wxs.ign.fr/io21v55kj0lbj44b9bvq9xtg/wmts",
    {
      params: {
        SERVICE: "WMTS",
        REQUEST: "GetCapabilities",
      },
      responseType: "text",
      auth: {
        username: "bruno.baudru",
        password: "NxuhOrXdXJoU43hmyWPi",
      },
    }
  );
  return new XMLParser().parse(response.data).Capabilities as CapabilitiesDto;
};

export interface CapabilitiesDto {
  "ows:ServiceIdentification": OwsServiceIdentificationDto;
  "ows:ServiceProvider": OwsServiceProviderDto;
  "ows:OperationsMetadata": OwsOperationsMetadataDto;
  Contents: ContentsDto;
}

interface ContentsDto {
  Layer: LayerDto[];
  TileMatrixSet: TileMatrixSetDto[];
}

interface LayerDto {
  "ows:Title": string;
  "ows:Abstract": string;
  "ows:Keywords": StyleOwsKeywordsDto;
  "ows:WGS84BoundingBox": OwsWGS84BoundingBoxDto;
  "ows:Identifier": string;
  Style: Style;
  Format: string;
  InfoFormat?: string[];
  TileMatrixSetLink: TileMatrixSetLinkDto;
}

interface Style {
  "ows:Title": string;
  "ows:Abstract": string;
  "ows:Keywords": StyleOwsKeywordsDto;
  "ows:Identifier": string;
  LegendURL: string;
}

interface StyleOwsKeywordsDto {
  "ows:Keyword": string;
}

interface TileMatrixSetLinkDto {
  TileMatrixSet: string;
  TileMatrixSetLimits: TileMatrixSetLimitsDto;
}

interface TileMatrixSetLimitsDto {
  TileMatrixLimits: TileMatrixLimitDto[];
}

interface TileMatrixLimitDto {
  TileMatrix: number;
  MinTileRow: number;
  MaxTileRow: number;
  MinTileCol: number;
  MaxTileCol: number;
}

interface OwsWGS84BoundingBoxDto {
  "ows:LowerCorner": string;
  "ows:UpperCorner": string;
}

interface TileMatrixSetDto {
  "ows:Identifier": string;
  "ows:SupportedCRS": string;
  TileMatrix: TileMatrixDto[];
}

interface TileMatrixDto {
  "ows:Identifier": number;
  ScaleDenominator: string;
  TopLeftCorner: TopLeftCornerDto;
  TileWidth: number;
  TileHeight: number;
  MatrixWidth: number;
  MatrixHeight: number;
}

/** 2 numbers given as a string "top left" */
type TopLeftCornerDto = string;

interface OwsOperationsMetadataDto {
  "ows:Operation": OwsOperationDto[];
}

interface OwsOperationDto {
  "ows:DCP": OwsDCPDto;
}

interface OwsDCPDto {
  "ows:HTTP": OwsHTTPDto;
}

interface OwsHTTPDto {
  "ows:Get": OwsGetDto;
}

interface OwsGetDto {
  "ows:Constraint": OwsConstraintDto;
}

interface OwsConstraintDto {
  "ows:AllowedValues": OwsAllowedValuesDto;
}

interface OwsAllowedValuesDto {
  "ows:Value": string;
}

interface OwsServiceIdentificationDto {
  "ows:Title": string;
  "ows:Abstract": string;
  "ows:Keywords": OwsServiceIdentificationOwsKeywordsDto;
  "ows:ServiceType": string;
  "ows:ServiceTypeVersion": string;
  "ows:Fees": string;
  "ows:AccessConstraints": string;
}

interface OwsServiceIdentificationOwsKeywordsDto {
  "ows:Keyword": string[];
}

interface OwsServiceProviderDto {
  "ows:ProviderName": string;
  "ows:ProviderSite": string;
  "ows:ServiceContact": OwsServiceContactDto;
}

interface OwsServiceContactDto {
  "ows:IndividualName": string;
  "ows:PositionName": string;
  "ows:ContactInfo": OwsContactInfoDto;
}

interface OwsContactInfoDto {
  "ows:Phone": OwsPhoneDto;
  "ows:Address": OwsAddressDto;
}

interface OwsAddressDto {
  "ows:DeliveryPoint": string;
  "ows:City": string;
  "ows:AdministrativeArea": string;
  "ows:PostalCode": number;
  "ows:Country": string;
  "ows:ElectronicMailAddress": string;
}

interface OwsPhoneDto {
  "ows:Voice": string;
  "ows:Facsimile": string;
}
