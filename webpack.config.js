const path = require("path");
var webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebPackPlugin = require("copy-webpack-plugin");

const environment =
  process.env.NODE_ENV === "production" ? "production" : "development";

const configs = {
  production: {
    deployedUrl: "https://zebulon39.gitlab.io/webmobilemap",
  },
  development: { deployedUrl: "http://localhost:8080" },
};

function modify(json) {
  let template = json.toString();

  for (const property in configs[environment]) {
    template = template.replace(
      new RegExp("\\$\\{" + property + "\\}", "g"),
      configs[environment][property]
    );
  }

  return template;
}

module.exports = {
  // webpack will take the files from ./src/index
  entry: "./src/index.ts",

  // and output it into /dist as bundle.js
  output: {
    path: path.join(__dirname, "/public"),
    filename: "bundle.js",
  },

  // adding .ts and .tsx to resolve.extensions will help babel look for .ts and .tsx files to transpile
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },

  module: {
    rules: [
      // we use babel-loader to load our jsx and tsx files
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },

      // css-loader to bundle all the css files into one file and style-loader to add all the styles  inside the style tag of the document
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
    }),
    new CopyWebPackPlugin({
      patterns: [
        {
          from: "./src/manifest.json",
          to: "manifest.json",
          transform(content, path) {
            return modify(content);
          },
        },
        { from: "./src/icon.png", to: "icon.png" },
        { from: "./src/no-wifi.png", to: "no-wifi.png" },
        { from: "./src/sw.js", to: "sw.js" },
      ],
    }),
  ],
};
